# Purge

This app deactivates unused accounts

## HowTo

A cron job must be running for this app to work. Users are processed 20 by 20.

* Go to the admin panel
* Activate the purge
* Select a time before the first warning
* Select a time before the deactivation
* The emails templates inside `apps/purge/templates` will be sent if users have not been connecting since the time you set

### Get the list of deactivated accounts
 `SELECT userid FROM oc_preferences WHERE appid='core' AND configkey='enabled' AND configvalue='false';`
 
### Remove all deactivated accounts
Just click the button. A cron job will be triggered.

## TODO

* Translate strings
* Make templates defined by the admin