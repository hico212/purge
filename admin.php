<?php
/**
 * ownCloud - purge.
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Thomas Citharel <tcit@tcit.fr>
 */
namespace OCA\Purge\AppInfo;

$app = new Application();
$controller = $app->getContainer()->query('SettingsController');

return $controller->displayPanel()->render();
