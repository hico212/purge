<?php

print_unescaped($l->t("Bonjour,\n\nVotre compte sur %s a été supprimé pour inactivité.", [$_['sitename']]));
print_unescaped("\n\n");
print_unescaped($l->t('Bonne journée !'));
print_unescaped("\n\n");
print_unescaped($theme->getName().'-'.$theme->getSlogan());
