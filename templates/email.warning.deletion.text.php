<?php
/** @var $l OC_L10N */
p($l->t("Bonjour,\n\nVotre compte sur %s n'a pas été utilisé depuis %s %s. ", [$_['sitename'], $_['time1'], $_['type1']]));

print_unescaped($l->t("ous essayons de faire de la place pour tout le monde en supprimant les comptes inutilisés.\n"));
print_unescaped($l->t("Votre compte sera automatiquement supprimé après %s %s si vous ne vous connectez pas entre temps. Si vous n'utilisez plus %s, merci de nous le notifier sur https://contact.framasoft.org/#framadrive.\n\n", [$_['time2'], $_['type2'], $_['sitename']]));
print_unescaped($l->t("Rappel : votre nom d'utilisateur est %s.", $_['username']));

// TRANSLATORS term at the end of a mail
p($l->t('Bonne journée !'));
?>

	--
<?php p($theme->getName().' - '.$theme->getSlogan()); ?>
<?php print_unescaped("\n".$theme->getBaseUrl());
